/* Highvoltage generator
 * Author : M R Maddumage 
 * madushar8472@gmail.com
 */
 
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);

int pot = A0;
int volt = A1;
int pushButton = 8;
int outSignal = 9;
int x;
float y;
int z;
int buttonState = 0;
int outSignal2 = 6;
float vol = 0;
// For initial idintification
//void setValues() {
//x = map(analogRead(pot), 0, 1024, 0, 200);
//}
void setup() {
  Serial.begin(9600);
  lcd.begin(16, 4);
  // For Push button
  pinMode(pushButton, OUTPUT);

  // Out Signal, can not change pin
  pinMode(outSignal, OUTPUT);
  pinMode(outSignal2, OUTPUT);//relay

  // For Duty variable volume controller. must be 5k ohms
  pinMode(pot, INPUT);

  // call above setValues void for set frequency
  // setValues();
lcd.setCursor(0, 1 );
  lcd.print("**High Voltage**");
  lcd.setCursor(0, 2 );
 lcd.print("Pulse generator");
delay(3000);
}

void loop() {
  x = map(analogRead(pot), 0, 1023, 0, 2000);///real time pulse width setting
  lcd.clear();
  lcd.setCursor(0, 1 );
  lcd.print("P width = " + String(x) + "us" + "   ");

  y = map(analogRead(volt), 0, 1023, 0, 3996); ///real time pulse width setting
  if (y < 990 && y > 100) {

    vol = (((140 - (0.126 * y)) + y)+10);
  }
  if (y >= 990 && y < 1010) {
    vol = y;
  }
  if (y > 1010) {

    vol = (((((0.18 * y) - 181) - y) * -1)+40);
  }
  if (y < 100) {
    vol = y;
  }
  Serial.println(x);
  
  lcd.setCursor(0, 2 );
  lcd.print("voltage = " + String(vol / 1000) + "kV" + "  ");
     delay(200);
 
  
  if (vol >= 2200) {
    digitalWrite(outSignal2, HIGH);// relay input
 lcd.setCursor(0, 3 );
  lcd.print("Over Voltage !!!");
delay(500);
  } else {
    digitalWrite(outSignal2, LOW); // relay input
  }

  switch (debounce(buttonState)) {

    case 1://UP
      digitalWrite(outSignal, HIGH);
      delayMicroseconds(x);
      // delay(x);
      break;
  }
  digitalWrite(outSignal, LOW);
}


int debounce(int state)
{
  int stateNow = digitalRead(pushButton);
  if (state != stateNow)
  {
    delay(100);
    stateNow = digitalRead(pushButton);
  }
  return stateNow;
}
